//! examples/01.rs
//! Example 01: Basic Fail usage

use triage_fail::Fail;

fn main() {
    println!("\n> @TODO");
    let result = format!("{:?}", Fail {});
    assert_eq!(result, "Fail");
    println!("{}", result);
}
