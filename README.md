# TRIAGE Fail

__A general purpose `Error` type for TRIAGE apps.__

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/op8d/triage_fail/>  
▶&nbsp; __Homepage:__ <https://op8d.gitlab.io/triage_fail/>  

Build and open the documentation:  
`cargo doc --no-deps --open`

## Examples

### Example 01: Basic Fail usage
   `cargo run --example 01`
